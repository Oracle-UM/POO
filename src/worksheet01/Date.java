package worksheet01;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class Date {
    public Date() {
        _date = LocalDate.now();
    }

    public Date(int year, int month, int day) {
        _date = LocalDate.of(year, month, day);
    }

    public int getYear() {
        return _date.getYear();
    }

    public int getMonth() {
        return _date.getMonthValue();
    }

    public int getDay() {
        return _date.getDayOfMonth();
    }

    public String toString() {
        return _date.toString();
    }

    public DayOfWeek getDayOfWeek(int year, int month, int day) {
        return _date.getDayOfWeek();
    }

    public void setYear(int year) {
        _date = _date.withYear(year);
    }

    public void setMonth(int month) {
        _date = _date.withMonth(month);
    }

    public void setDay(int day) {
        _date = _date.withDayOfMonth(day);
    }

    private LocalDate _date;
}
